<?php

use Illuminate\Database\Seeder;
use App\User;

class CsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = explode(',', env('LIST_CUSTOMER_SERVICE'));
        $no = 0;
        foreach($users as $user){
            User::firstOrCreate([
                'phone' => '62' . substr(str_replace('-','',$user), -11),
            ],
            [
                'name' => 'admin-' . ++$no,
                'email' => 'admin-' . $no . '@admin.com',
                'phone' => '62' . substr(str_replace('-','',$user), -11),
                'password' => bcrypt('password'),
                'is_maid' => 1
            ]);
        }
        dd($users);
    }
}
