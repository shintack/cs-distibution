<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('start-chat');
});

Route::post('/', 'CustomerServiceController@store');

Route::get('/offline',function () {
    return view('offline');
});

// Route::group(['middleware' => ['web']], function () {
//     Route::post('/customer-service', 'CustomerServiceController@store');
// });

Auth::routes();

// Route::get('/user', 'HomeController@user')->name('user');
Route::group(['middleware' => ['auth']], function () {
    // Route::get('/users', 'UserController@index')->name('user');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('users', 'UserController');
    Route::get('/users-set-status/{id}', 'UserController@setStatus')->name('set.status');
    Route::get('/users-set-schedule/{id}', 'UserController@setSchedule')->name('set.schedule');
    Route::get('/users-delete-schedule/{id}', 'UserController@deleteSchedule')->name('delete.schedule');
    Route::get('/users-restore-schedule/{id}', 'UserController@restoreSchedule')->name('restore.schedule');
    Route::post('/users-post-schedule/{id}', 'UserController@setPostSchedule')->name('post.schedule');

});

Route::get('/customer-service', 'HomeController@cs')->name('customer.service');
