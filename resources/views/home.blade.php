@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Jumlah Server :  {{ auth()->user()->countServer() }} <br>
                    Jumlah Customer :  {{ auth()->user()->countCustomer() }} <br>
                    Jumlah Chat yang masuk hari ini :  {{ $cs->customerToday() }} <br>
                    Jumlah Chat yang masuk :  {{ $cs->count() }} <br>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
