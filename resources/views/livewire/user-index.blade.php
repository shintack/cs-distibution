<div>
    @if (session('message'))
        <div class="alert alert-success" role="alert">
            {{ session('message') }}
        </div>
    @endif

    @if ($updateStatus)
    <livewire:user-update>
    @else
    <livewire:user-create>
    @endif
    <hr>
    <form>
        <div class="row mb-2">
            <div class="col-2">
                <select wire:model="paginate" name="paginate" class="form-control">
                    @foreach (explode(',', env('PAGINATE', '5,10,20,50')) as $p)
                        <option value="{{ $p }}">{{ $p }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col">
                <input wire:model="search" type="text" class="form-control" placeholder="Search user name">
            </div>
        </div>
    </form>

    <div class="table-reponsive">
        <table class="table table-hovered">
            <thead class="thead-dark">
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                    <tr>
                        <td scope="row"> {{ $user->id }} </td>
                        <td> {{ $user->name }} </td>
                        <td> {{ $user->email }} </td>
                        <td> {{ $user->phone }} </td>
                        <td>
                            <button wire:click="getData({{ $user->id }})" class="btn btn-sm btn-success text-white">Edit</button>
                            <button wire:click="destroy({{ $user->id }})" class="btn btn-sm btn-danger text-white">Delete</button>
                        </td>
                    </tr>

                @endforeach
            </tbody>
        </table>
    </div>
    {{ $users->links() }}
</div>
