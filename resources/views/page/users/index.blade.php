@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Users
                    <a href="{{ route('users.create') }}" class="float-right btn btn-primary">Add</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="" method="get">
                        <div class="row">
                            <div class="col-md-8 form-inline">
                                <div class="form-group">
                                    <select name="is_maid" id="" class="form-control">
                                        <option value=""> Is Admin </option>
                                        <option value="1" {{ request('is_maid') == '1' ? 'selected' : '' }} > Ya </option>
                                        <option value="0" {{ request('is_maid') == '0' ? 'selected' : '' }} > Tidak </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select name="is_available" id="" class="form-control">
                                        <option value=""> Is Available </option>
                                        <option value="1" {{ request('is_available') == '1' ? 'selected' : '' }} > Ya </option>
                                        <option value="0" {{ request('is_available') == '0' ? 'selected' : '' }} > Tidak </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select name="day" id="" class="form-control">
                                        <option value=""> Hari </option>
                                        @foreach([1,2,3,4,5,6,0] as $i)
                                            <option value="{{ $i }}" {{ request('day') == "$i" ? 'selected' : '' }} > {{ dayName($i) }} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <input type="text" class="form-control" name="search" placeholder="Pencarian" value="{{ request('search') }}">
                            </div>
                            <div class="col-md-1">
                                <button class="btn btn-primary">Filter</button>
                            </div>
                        </div>
                    </form>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name / Email / Phone</th>
                                    <th>Schedule</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    @include('page.users.child.row')
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
