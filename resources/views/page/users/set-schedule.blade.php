@extends('layouts.app')

@section('content')
<div class="container">
<form action="{{ route('post.schedule', $user->id) }}" method="POST">

    <div class="row justify-content-center">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Data User
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


                        @csrf
                        <div class="form-group">
                            <label for="">Name : </label>
                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ isset($user->name) ? $user->name :  old('name') }}">
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Email : </label>
                            <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ isset($user->email) ? $user->email : old('email') }}">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Phone : </label>
                            <input type="number" name="phone" class="form-control @error('phone') is-invalid @enderror" value="{{ isset($user->phone) ? $user->phone : old('phone') }}">
                            @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary ">Update User</button>
                        </div>

                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Set Schedule Users
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="form-group">
                        <label for="">Hari : </label>
                        <select name="day" id="" class="form-control">
                            <option value="1">Senin</option>
                            <option value="2">Selasa</option>
                            <option value="3">Rabu</option>
                            <option value="4">Kamis</option>
                            <option value="5">Jumat</option>
                            <option value="6">Sabtu</option>
                            <option value="0">Minggu</option>
                        </select>
                        @error('day')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Jam Mulai : </label>
                        <input type="time" name="start_at" class="form-control">
                        @error('start_at')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Jam Akhir : </label>
                        <input type="time" name="end_at" class="form-control">
                        @error('start_at')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group text-right">
                        <button class="btn btn-primary ">Save Schedule</button>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row mt-3">
        <div class="col-md-12">
             <div class="card">
                <div class="card-header">
                    Set Schedule Users
                </div>

                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Hari</th>
                                <th>Jam Mulai</th>
                                <th>Jam Akhir</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ($user->allSchedules->count() > 0)
                                @foreach ($user->allSchedules as $schedule)
                                    <tr>
                                        <td> {{ dayName($schedule->day) }} </td>
                                        <td> {{ $schedule->start_at }} </td>
                                        <td> {{ $schedule->end_at }} </td>
                                        <td>
                                            @if ($schedule->deleted_at)
                                                <a href="{{ route('restore.schedule', $schedule->id) }}" class="btn btn-warning btn-sm">Restore</a>
                                            @else
                                                <a href="{{ route('delete.schedule', $schedule->id) }}" class="btn btn-danger btn-sm">Delete</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else

                            @endif
                        </tbody>
                    </table>
                </div>
             </div>

        </div>
    </div>
</form>

</div>
@endsection
