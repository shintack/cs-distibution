<tr>
    <td> {{ $user->id }} </td>
    <td> {{ $user->name }} <br> {{ $user->email }} <br> {{ $user->phone }} </td>
    <td>
        @if ($user->schedules->count() > 0)
            @foreach ($user->schedules as $sc)
                <button class="btn btn-sm btn-secondary" data-toggle="tooltip" title="{{ $sc->start_at . ' s/d ' . $sc->end_at }}" > {{ dayName($sc->day) }} </button>
            @endforeach
        @endif
    </td>
    <td class="text-right">
        @if ($user->is_maid)
            <a href="{{ route('set.status', [ 'id' => $user->id, 'is_maid' => 0 ] ) }}" class="btn btn-sm btn-primary">Admin</a>
        @else
            <a href="{{ route('set.status', [ 'id' => $user->id, 'is_maid' => 1 ] ) }}" class="btn btn-sm btn-danger">Set Admin</a>
        @endif
        @if ($user->is_available)
            <a href="{{ route('set.status', [ 'id' => $user->id, 'is_available' => 0 ] ) }}" class="btn btn-sm btn-primary">Available</a>
        @else
            <a href="{{ route('set.status', [ 'id' => $user->id, 'is_available' => 1 ] ) }}" class="btn btn-sm btn-danger">Set Avail</a>
        @endif

        <a href="{{ route('set.schedule', $user->id ) }}" class="btn btn-sm btn-success">Set Schedule</a>

        {{-- <a href="{{ route('users.edit', $user->id ) }}" class="btn btn-sm btn-success">Edit</a>
        <a href="{{ route('users.destroy', $user->id ) }}" class="btn btn-sm btn-danger">Hapus</a> --}}


    </td>
</tr>
