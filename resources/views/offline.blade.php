<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    {{-- <link rel="stylesheet" href="{{ url('/floating-whatsapp/floating-wpp.css') }}">
    <script type="text/javascript" src="{{ url('/floating-whatsapp/floating-wpp.js') }}"></script> --}}

    <title>Offline</title>
    <style>
        .pre-footer {
            width: 100%;
            background-color: #273443;
        }

        .pre-footer h6 {
            color: rgba(255, 255, 255, .6);
        }

        .list {
            font-size: small;
            list-style-type: none;
            line-height: 20px;
            padding-left: 0px;
        }

        .footer {
            /* position: absolute; */
            bottom: 0;
            width: 100%;
            /* height: 60px; */
            /* Set the fixed height of the footer here */
            line-height: 60px;
            /* Vertically center the text there */
            background-color: #232f3c;
        }

    </style>

</head>

<body style="background-color: #f8f9fa;">

    <nav class="navbar navbar-expand-lg navbar-dark"
        style="background-color: #1ebea5 !important; color: white !important;">
        <div class="container">
            <a class="navbar-brand" href="/">
                <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/WhatsApp.svg/480px-WhatsApp.svg.png"
                    width="30" height="30" class="d-inline-block align-top" alt=""> <b> WhatsApp </b>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">WHATSAPP WEB <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="#">FITUR</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="#">UNDUH</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="#">KEAMANAN</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="#">PUSAT BANTUAN</a>
                    </li>
                    <li class="nav-item dropdown active">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            ID
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">EN</a>
                            <!-- <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a> -->
                        </div>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                    </li> -->
                </ul>
            </div>

        </div>

    </nav>
    <div class="container bg-white p-3">
        <div class="row">
            <div class="col-md-12" style="min-height: 300px;">
                <h3>Maaf, saat ini admin sedang offline. Silahkan mencoba kembali disaat jam kerja </h3>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="pre-footer ">
            <div class="container ">
                <div class="row text-white pt-3 ">
                    <div class="col-sm-3 ">
                        <h6>WHATSAPP</h6>
                        <ul class="list ">
                            <li> Fitur </li>
                            <li> Keamanan </li>
                            <li> Unduh </li>
                            <li> WhatsApp Web</li>
                            <li> Bisnis </li>
                            <li> Privasi </li>
                        </ul>
                    </div>
                    <div class="col-sm-3 ">
                        <h6>PERUSAHAAN</h6>
                        <ul class="list ">
                            <li> Info </li>
                            <li> Karir </li>
                            <li> Pusat Merek </li>
                            <li> Tetap Terhubung </li>
                            <li> Blog </li>
                            <li> Cerita WhatsApp </li>
                        </ul>
                    </div>
                    <div class="col-sm-3 ">
                        <h6>UNDUH</h6>
                        <ul class="list ">
                            <li> Mac/PC </li>
                            <li> Android </li>
                            <li> iPhone </li>
                        </ul>
                    </div>
                    <div class="col-sm-3 ">
                        <h6>DUKUNGAN</h6>
                        <ul class="list ">
                            <li> Pusat Bantuan </li>
                            <li> Android </li>
                            <li> iPhone </li>
                        </ul>

                    </div>
                </div>

            </div>
        </div>

        <div style="background-color: #232f3c ">
            <div class="container ">
                <span class="text-muted ">Contact admin via WhatsApp.</span>
            </div>

        </div>
    </footer>

    <div class="floating-wpp"></div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js "
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n " crossorigin="anonymous ">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js "
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo " crossorigin="anonymous ">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js "
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6 " crossorigin="anonymous ">
    </script>

    {{-- <script type="text/javascript" src="{{ url('/floating-whatsapp/floating-wpp.js') }}"></script> --}}
    {{--
    <script>
        (function() {
            // $('.floating-wpp').floatingWhatsApp({
            //     phone: '99999999999',
            //     popupMessage: 'Welcome to iamrohit.in',
            //     showPopup: true,
            //     position: 'left', // left or right
            //     autoOpen: false, // true or false
            //     //autoOpenTimer: 4000,
            //     message: 'Hello how are you my name is rohit',
            //     //headerColor: 'orange', // enable to change msg box color
            //     headerTitle: 'Whatsapp Message Box',
            //     postUrl: 'http://cs-academy.local?name=nama user&email=email@email.com&phone=08123123'
            // });
        })();
    </script>
    --}}
</body>

</html>
