<?php

namespace App\Http\Controllers;

use App\Customer;
use App\CustomerService;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $cs = new CustomerService();
        return view('home', [
            'cs' => $cs
        ]);
    }

    public function user()
    {
        return view('user');
    }

    public function cs()
    {
        return view('customer-service');
    }

    public function offline()
    {
        return view('offline');
    }

}
