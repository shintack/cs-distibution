<?php

namespace App\Http\Controllers;

use App\Schedule;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::filter($request->all())->with('schedules')->latest()->paginate(10);
        $users->appends([
            'search' => $request->search,
            'is_maid' => $request->is_maid
        ]);
        return view('page.users.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('page.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->validateData($request);

        User::create($data);

        return redirect()->back()->with(['status', 'Success create user']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        dd($id, $request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function setStatus(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update($request->all());
        return redirect()->back();
    }

    public function setSchedule($id)
    {
        $user = User::findOrFail($id);
        return view('page.users.set-schedule', [ 'user' => $user ]);
    }

    public function setPostSchedule(Request $request, $id)
    {
        $data = $this->validateData($request);

        $user = User::with('schedules')->findOrFail($id);
        $user->update($data);

        if ($request->day || $request->start_at || $request->end_at) {
            $schedule = $request->validate([
                'day' => 'required',
                'start_at' => 'required',
                'end_at' => 'required',
            ]);

            Schedule::updateOrCreate([
                'user_id' => $user->id,
                'day' => $schedule['day'],
            ],[
                'start_at' => $schedule['start_at'],
                'end_at' => $schedule['end_at'],
            ]);
        }

        return redirect()->back()->with(['status', 'Success create user']);

    }

    public function deleteSchedule($id)
    {
        Schedule::findOrFail($id)->delete();
        return redirect()->back()->with(['status', 'Success delete schedule']);
    }

    public function restoreSchedule($id)
    {
        Schedule::withTrashed()->findOrFail($id)->restore();
        return redirect()->back()->with(['status', 'Success restore schedule']);
    }

    private function validateData(Request $request)
    {
        return $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric',
        ]);
    }

}
