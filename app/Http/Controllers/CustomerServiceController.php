<?php

namespace App\Http\Controllers;

use App\CustomerService;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CustomerServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'problem' => 'required',
            'message' => 'required',
            'url' => '',
        ]);

        $data['password'] = 'passajadeh';

        // cek apakah email sudah terdaftar
        $user = User::firstOrCreate(['email' => $data['email']], $data);
        // dd($user);
        if ($user) {
            /**
             * TO DO LOGIC SET ADMIN
             */
            // 1. cek apakah hari ini sudah ada yg chating?
            $cek = CustomerService::whereDate('created_at', Carbon::today())->get();
            // dd($cek);
            if ($cek->count() == 0) {
                // 2. jika tidak ada maka, ambil satu customer service -> maka layani
                $server = new User();
                $servicer = $server->startServicer();
                // dd($servicer);
                if($servicer){
                    // $servicer = User::where('is_maid', 1)->limit(1)->inRandomOrder()->get()->first();
                    $inServe = $this->setServicer($user, $servicer, $data);
                    // redirect to wa
                }else{
                    // tidak ada admin yg online di jam tersebut
                    return redirect('/offline');
                }
            }
            // 3. jika sudah ada maka,
            else {
                // dd($user);
                // 3.1 cek apakah user tersebut pernah di layanin hari itu
                if ($user->inServiceToday) {
                    // 3.2 jika iya, maka arahkan ke servicer nya
                    $inServe = $user->inServiceToday;
                    // cek apakah admin masih dalam jam kerja?
                    $servicer = User::find($inServe->service_id);
                    // dd($servicer->onWorkingHour());
                    // jika admin diluar jam kerja
                    if(!$servicer->onWorkingHour()){
                        // cari admin lain
                        $server = new User();
                        $servicer = $server->nextServicer();
                        if ($servicer) {
                            // $servicer = User::where('is_maid', 1)->limit(1)->orderBy('count_servicing', 'asc')->get()->first();
                            $inServe = $this->setServicer($user, $servicer, $data);
                        } else {
                            // tidak ada admin yg online di jam tersebut
                            return redirect('/offline');
                        }
                    }
                    // jika msh online bypass to wa
                } else {
                    // 3.3 jika blm, maka carikan servicer lainnya yg kosong
                    $server = new User();
                    $servicer = $server->nextServicer();
                    if ($servicer) {
                        // $servicer = User::where('is_maid', 1)->limit(1)->orderBy('count_servicing', 'asc')->get()->first();
                        $inServe = $this->setServicer($user, $servicer, $data);
                    } else {
                        // tidak ada admin yg online di jam tersebut
                        return redirect('/offline');
                    }
                }
            }

            $phoneServer = $inServe->servicer->phone;

            $text = 'Halo, saya ' . $user->name . ', ingin bertanya tentang ' . $data['problem'] . ' di ' . $data['url'] . ' masalahnya adalah ' . $data['message'];

            // $linkWa = "https://wa.me/{$phoneServer}?text={$text}";

            $linkWa = "https://api.whatsapp.com/send?phone={$phoneServer}&text={$text}";

            return redirect()->to($linkWa);
        }
        // jika gagal maka arahkan ke tawk to
        return redirect()->to('https://tawk.to/chat/5ea0f8ad35bcbb0c9ab3cc99/default');
    }

    private function setServicer($customer, $servicer, $data)
    {
        $data['customer_id'] = $customer->id;
        $data['service_id'] = $servicer->id;
        $data['problem'] = json_encode([
            'problem' => $data['problem'],
            'from' => $data['url'],
        ]);
        $inServe = CustomerService::create($data);
        if ($inServe) {
            $servicer->increment('count_servicing'); // = $servicer->count_servicing + 1;
            $servicer->save();
        }
        return $inServe;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomerService  $customerService
     * @return \Illuminate\Http\Response
     */
    public function show(CustomerService $customerService)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomerService  $customerService
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerService $customerService)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomerService  $customerService
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomerService $customerService)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomerService  $customerService
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomerService $customerService)
    {
        //
    }
}
