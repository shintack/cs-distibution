<?php

namespace App\Http\Livewire;

use App\Student;
use Livewire\Component;
use Livewire\WithPagination;

class StudentIndex extends Component
{
    use WithPagination;

    public $search;
    public $page = 1;

    protected $updatesQueryString = [
        ['page' => ['except' => 1]],
        ['search' => ['except' => '']],
    ];

    protected $listeners = [
        'studentAdded',
    ];

    public function studentAdded()
    {
        # code...
    }

    public function render()
    {
        if ($this->search !== null) {
            $students = Student::where('name', 'like', '%' . $this->search . '%')
            ->latest()
            ->paginate(5);
            $this->page = 1;
        }else{
            $students = Student::latest()->paginate(5);
        }

        return view('livewire.student-index', [
            'students' => $students,
        ]);
    }
}
