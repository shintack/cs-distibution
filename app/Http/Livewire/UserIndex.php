<?php

namespace App\Http\Livewire;

use App\User;
use Livewire\Component;
use Livewire\WithPagination;

class UserIndex extends Component
{
    use WithPagination;

    public $selectedData;
    public $updateStatus = false;
    public $search = '';
    public $paginate = 5;
    public $page = 1;

    protected $updatesQueryString = [
        ['page' => ['except' => 1]],
        ['search' => ['except' => '']],
    ];

    protected $listeners = [
        'storedData',
        'updatedData',
    ];

    public function render()
    {
        if ($this->search !== null) {
            $users = User::where('name', 'like', '%' . $this->search . '%')
                ->latest()->paginate($this->paginate);
            $this->page = 1;
        }else{
            $users = User::latest()->paginate($this->paginate);
        }
        return view('livewire.user-index', [
            'users' => $users
        ]);
    }

    public function getData($id)
    {
        $this->updateStatus = true;
        $this->selectedData = User::findOrFail($id);
        $this->emit('getData', $this->selectedData);
    }

    public function destroy($id)
    {
        $data = User::findOrFail($id);
        $data->delete();
        session()->flash('message', 'Success deleted data');
    }

    public function storedData()
    {
        session()->flash('message', 'Success stored data');
    }

    public function updatedData()
    {
        $this->updateStatus = false;
        session()->flash('message', 'Success updated data');
    }

}
