<?php

namespace App\Http\Livewire;

use App\User;
use Livewire\Component;

class UserCreate extends Component
{
    public $name;
    public $email;
    public $phone;

    public function resetInput()
    {
        $this->name = '';
        $this->email = '';
        $this->phone = '';
    }

    public function render()
    {
        return view('livewire.user-create');
    }

    public function store()
    {
        $data = $this->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ]);

        User::create($data);

        $this->resetInput();

        $this->emit('storedData');
    }
}
