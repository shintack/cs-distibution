<?php

namespace App\Http\Livewire;

use App\User;
use Livewire\Component;

class UserUpdate extends Component
{
    public $userId;
    public $name;
    public $email;
    public $phone;

    protected $listeners = [
        'getData' => 'showData'
    ];

    public function resetInput()
    {
        $this->name = '';
        $this->email = '';
        $this->phone = '';
    }

    public function render()
    {
        return view('livewire.user-update');
    }

    public function showData($data)
    {
        $this->userId = $data['id'];
        $this->name = $data['name'];
        $this->email = $data['email'];
        $this->phone = $data['phone'];
    }

    public function update()
    {
        $this->validate([
            'userId' => 'required|numeric',
            'name' => 'required|min:3',
            'email' => 'required|email',
            'phone' => 'required|max:15',
        ]);

        if ($this->userId) {
            $data = User::findOrFail($this->userId);
            $data->update([
                'name' => $this->name,
                'email' => $this->email,
                'phone' => $this->phone,
            ]);

            $this->resetInput();

            $this->emit('updatedData');
        }
    }
}
