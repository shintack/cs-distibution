<?php

if (!function_exists('dayName')) {
    /**
     * Returns a human readable file size
     *
     * @param integer $bytes
     * Bytes contains the size of the bytes to convert
     *
     * @param integer $decimals
     * Number of decimal places to be returned
     *
     * @return string a string in human readable format
     *
     * */
    function dayName($day)
    {
        switch ($day) {
            case '1':
                return 'Senin';
                break;
            case '2':
                return 'Selasa';
                break;
            case '3':
                return 'Rabu';
                break;
            case '4':
                return 'Kamis';
                break;
            case '5':
                return 'Jumat';
                break;
            case '6':
                return 'Sabtu';
                break;
            case '0':
                return 'Minggu';
                break;

            default:
                return '-';
                break;
        }
    }
}
