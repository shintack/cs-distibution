<?php

namespace App\ModelFilters;

use EloquentFilter\ModelFilter;

class UserFilter extends ModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relationMethod => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = [];

    public function search($value)
    {
        return $this->where(function ($q) use ($value) {
            return $q->where('name', 'like', "%$value%")->orWhere('email', 'like', "%$value%")->orWhere('phone', 'like', "%$value%");
        });
    }

    public function isMaid($value)
    {
        return $this->where('is_maid', $value);
    }

    public function isAvailable($value)
    {
        return $this->where('is_available', $value);
    }

    public function day($value)
    {
        return $this->whereHas('schedules', function ($q) use ($value) {
            return $q->where('day', $value);
        });
    }

}
