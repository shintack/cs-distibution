<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use EloquentFilter\Filterable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes, Filterable;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->is_maid = 0;
            $model->is_available = 0;
            $model->count_servicing = 0;
            $model->password = bcrypt('password');
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'password',
        'count_servicing',
        'is_maid',
        'is_available',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function modelFilter()
    {
        return $this->provideFilter(\App\ModelFilters\UserFilter::class);
    }

    public function inServiceToday()
    {
        return $this->hasOne(CustomerService::class, 'customer_id', 'id')->whereDate('created_at', Carbon::today());
    }

    public function inServices()
    {
        return $this->belongsTo(CustomerService::class, 'service_id', 'id');
    }

    public function history()
    {
        return $this->belongsTo(CustomerService::class, 'service_id', 'id');
    }

    public function schedules()
    {
        return $this->hasMany('App\Schedule', 'user_id');
    }

    public function allSchedules()
    {
        return $this->hasMany('App\Schedule', 'user_id')->withTrashed();
    }

    public function countServer()
    {
        return $this->whereIsMaid(1)->count();
    }

    public function countCustomer()
    {
        return $this->whereIsMaid(0)->count();
    }

    public function startServicer()
    {
        // reset count servicing
        $this->where('count_servicing', '>', 0)->update(['count_servicing' => 0]);

        // ambil user yg online sekarang
        return $this->where([
            'is_maid' => 1,
            'is_available' => 1,
        ])->whereHas('schedules', function ($q) {
            return $q->where('day', date('w'))
                ->whereTime('start_at', '<=', date('H:i:s'))
                ->whereTime('end_at', '>=', date('H:i:s'));
        })->limit(1)->inRandomOrder()->get()->first();
    }

    public function nextServicer()
    {
        // reset count servicing
        // $this->where('is_available', 1)->update(['count_servicing' => 1]);

        // ambil user yg online sekarang
        return $this->where([
            'is_maid' => 1,
            'is_available' => 1,
        ])->whereHas('schedules', function ($q) {
            return $q->where('day', date('w'))
                ->whereTime('start_at', '<=', date('H:i:s'))
                ->whereTime('end_at', '>=', date('H:i:s'));
        })->limit(1)
        ->orderBy('count_servicing', 'asc')
        ->get()->first();
    }

    public function onWorkingHour()
    {
        return $this->whereHas('schedules', function ($q) {
            return $q->where('day', date('w'))
                ->whereTime('start_at', '<=', date('H:i:s'))
                ->whereTime('end_at', '>=', date('H:i:s'));
        })->first();
    }

}
