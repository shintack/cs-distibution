<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CustomerService extends Model
{
    protected $fillable = [
        'customer_id',
        'service_id',
        'problem',
        'message',
        'solved',
    ];

    public function servicer()
    {
        return $this->belongsTo(User::class, 'service_id', 'id');
    }

    public function customer()
    {
        return $this->belongsTo(User::class, 'customer_id', 'id');
    }

    public function customerToday()
    {
        return $this->whereDate('created_at', Carbon::today())->count();
    }
}
